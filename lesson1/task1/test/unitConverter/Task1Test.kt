package unitConverter

import org.hyperskill.hstest.dev.stage.BaseStageTest
import org.hyperskill.hstest.dev.testcase.CheckResult
import org.hyperskill.test_utils.testcase_helpers.OutputClue
import org.hyperskill.test_utils.testcase_helpers.authorsCaseFromFun
import org.hyperskill.test_utils.testcase_helpers.ciphered
import org.hyperskill.test_utils.WordComparer
import unitConverter.authors.Authors
import kotlin.reflect.jvm.javaMethod


/** TestCase, based on authors solution output. */
fun authorsCase(input: String, isPrivate: Boolean = false) = authorsCaseFromFun(Authors::solve, input, isPrivate)

class Task1Test : BaseStageTest<OutputClue>(::main.javaMethod) {
    init {
        isTestingMain = true
    }
    
    override fun generateTestCases() = listOf(
            authorsCase("25"),
            authorsCase("26", isPrivate = true),
            authorsCase("1"),
            authorsCase("0"),
            authorsCase("0")
    )
    
    override fun check(reply: String, clue: OutputClue): CheckResult {
        // compare output the clue output and reply with our custom comparer.
        val checkResult = WordComparer(clue.output, reply).compare()
    
        if (clue.isPrivate)
            return checkResult.ciphered()
        return checkResult
    }
}