package unitConverter.authors

import java.io.*
import java.util.*


object Authors {
    fun solve(sin: Scanner, sout: PrintStream) {
        
        sout.print("Enter a number of kilograms: ")
        
        val x = sin.nextInt()
        
        sout.println("$x kilograms is ${x * 1000} grams")
    }
    
    @JvmStatic
    fun main(args: Array<String>) {
        solve(Scanner(System.`in`), System.out)
    }

}
