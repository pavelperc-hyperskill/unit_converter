package org.hyperskill.test_utils.tester_tests

import org.junit.Assert.*
import org.junit.Test
import org.hyperskill.test_utils.WordComparer

class WordComparerTest {
    
    @Test
    fun testTokenizer() {
        
        // TODO
    }
    
    private fun WordComparer.shouldBeTrue() {
        val checkResult = compare()
        println("CheckResult: ${checkResult.isCorrect}, ${checkResult.feedback}")
        assertTrue(checkResult.isCorrect)
    }
    
    private fun WordComparer.shouldBeFalse() {
        val checkResult = compare()
        println("CheckResult: ${checkResult.isCorrect}, ${checkResult.feedback}")
        assertFalse(checkResult.isCorrect)
    }
    
    private infix fun Any?.shouldEqual(other: Any?) {
        assertEquals(this, other)
    }
    
    
    @Test
    fun compareByWordsTest() {
        WordComparer(
                "25 kg is 25000=0.123456 gr!!",
                "25 kg is 25000=0.12 gr!!",
                roundDoubleTo = 2
        ).shouldBeTrue()
        
        WordComparer(
                "25 kg is 25000=0.123456 gr!!",
                "25 kg is 25000=0.12 gr!!",
                roundDoubleTo = 3
        ).shouldBeFalse()
        
        WordComparer(
                "25 kg is 25000=0.12 gr!!",
                "25 kg is 25000.0=0.12 gr!!",
                integersAsDoubles = false
        ).shouldBeFalse()
        
        WordComparer(
                "25 kg is 25000=0.12 gr!!",
                "25 kg is 25000.0=0.12 gr!!",
                integersAsDoubles = true
        ).shouldBeTrue()
        
        // test scientific format.
        WordComparer(
                "25 kg is 25=0.5E+3 gr!!",
                "25 kg is 25=500.0 gr!!"
        ).shouldBeTrue()
        
        WordComparer(
                "25 kg is 25=2.0e-3 gr!!",
                "25 kg is 25=0.002 gr!!"
        ).shouldBeTrue()
        
        WordComparer(
                "hello\n blalbalba\n end", // authors
                "hello\n err2 err 3\n err4\n end",
                trimErrorOnlyByLine = true
        ).compare().feedback shouldEqual "Your line \" err2 err 3\"\n" +
                "doesn't match with \" blalbalba\"\n" +
                "in parts \"err\" and \"blalbalba\"."
    
    
    
        WordComparer(
                "Some text with optional dot.",
                "Some text; with optional dot",
                moreIgnoredSymbolsRegex = """\.;"""
        ).shouldBeTrue()
        
    }
}